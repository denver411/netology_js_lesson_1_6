'use strict'

//Задача № 1
const taxRate = 0.73;
let taxSumTotal = 0,
    taxSumTotalV2 = 0;

function taxes(orderSum) {
    let taxSum = 0;
    taxSum = orderSum * taxRate;
    taxSumTotal += taxSum;
    return taxSumTotal;
}

function taxesV2(orderSum) {
    let taxSum = 0;
    (function () {
        taxSum = orderSum * taxRate;
        taxSumTotalV2 += taxSum;
        return taxSumTotalV2;
    })()
}

taxes(100);
taxes(200);
taxes(300);

console.log(`Налог с продаж (${taxRate*100} %), к оплате: ${taxSumTotal} Q`);
taxesV2(100);
taxesV2(200);
taxesV2(300);
console.log(`Вариант 2: Налог с продаж (${taxRate*100} %), к оплате: ${taxSumTotalV2} Q`);

//Задача № 2
console.log('\n');
let packageInStock = 30;

function packageOrder(a, b, c) {
    let packageSquare = 2 * (a * b + a * c + b * c);
    if (packageInStock > packageSquare) {
        packageInStock -= packageSquare;
        console.log(`Заказ (${a}/${b}/${c} м) упакован, осталось упаковочной бумаги ${Math.round(packageInStock)} м2`);
        return true;
    } else {
        console.log(`Заказ (${a}/${b}/${c} м) не упакован, осталось упаковочной бумаги ${Math.round(packageInStock)} м2`);
        return false;
    }
}

packageOrder(1, 0.8, 1);
packageOrder(1, 0.5, 0.3);
packageOrder(1.2, 0.5, 0.8);
packageOrder(2.5, 1.5, 2.3);

//Задача № 3
console.log('\n');
let teleports = [7, 2, 1, 4, 8];
let counters = [];
for (let i = 0; i < teleports.length; i++) {
    counters[i] = function () {
        if (teleports[i] > 1) {
            console.log(`Телепорт ${i+1} использован, заряд — ${--teleports[i]} единиц`);
        } else if (teleports[i] === 1) {
            console.log(`Телепорт ${i+1} использован, заряд — ${--teleports[i]} единиц, требуется перезарядка!`);
        } else {
            console.log(`Телепорт ${i+1} недоступен, перезаряжается`)
        }
    }
}
counters[3]();
counters[4]();
counters[3]();
counters[3]();
counters[3]();
counters[3]();
counters[3]();